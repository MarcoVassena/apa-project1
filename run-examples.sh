#! /bin/sh

RUN='dist/build/main/main'
EXAMPLE='examples'

for name in `find $EXAMPLE -name \*.php`; do
  $RUN $name > ${name}.log
done
