<?php

function fib ( $x ){
  if ($x <= 2){
    return 1;
  }
  else {
    $u = fib($x - 2);
    $v = fib($x - 1);
    return ($u + $v);
  }
}

$r = fib(5);

?>
