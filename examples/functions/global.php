<?php
  
  function inc() {
  global $x;
  echo '$x is ' . $x . "\n";
  $x++;
  return;
  }
  
  $x = NULL;
  inc();    // After this call $x is available here with value 1
  $y = $x;
  echo '$y is ' . $y . "\n";
  inc();
  $z = $x;
  echo '$z is ' . $z . "\n";

?>
