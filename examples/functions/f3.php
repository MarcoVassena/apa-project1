<?php

// Without context I expect much poisioning

function id( $x ){
  return $x;
}

$a = id( 0 );
$b = id( 'foo' );
$c = id( NULL );
$d = id( $a );
$x = 0;
$y = id( $x );

?>
