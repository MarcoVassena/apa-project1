<?php

function f($a)
{
    if ($a < 20) {
        echo "$a\n";
        return 22;
    }
    return 33;
}

$y = 2;
$x = f(30, $y++);
echo $x;
?>
