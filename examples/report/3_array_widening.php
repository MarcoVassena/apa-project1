<?php

// Without widening the analysis would loop trying to compute the type of $a after the loop,
// which is the join of the type of $a with its own type nested in a TArray.

$a[1] = 1;
while (true) {
    $a[1] = $a;
}

// The results depend on the parameter used in the array widening (mergeArrays)
// Parameter 3 : 
// Expected   [ a => {TArray {TNum, TArray {TNum, TArray {TNum, TArray top}}}} ] 
?>
