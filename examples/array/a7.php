<?php

$a[0][0] = True;
$b = $a[0];
$c = $b[0];

// Arrays type themselves are propagated correctly:
// Expected
// $a -> TArray { TArray { TBool }}
// $b -> {TArray {TBool}
// $c -> {TBool}

?>
