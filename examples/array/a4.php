<?php

if (TRUE) {
  $a[0] = 1;
} else {
  $a[0] = False;
}

// $a is TArray { TNum, TBool } because it could contain both

$b = $a[0];

// The information about the content of $a is propagated correctly to $b
// which could be both TNum and TBool

?>
