<?php

$c[0][0][0][0] = NULL;  // Drop further information about array content after the 3rd dimension

// Without widening the analysis would loop trying to compute the type of $a after the loop,
// which is the join of the type of $a with its own type nested in a TArray.

$a[1] = 1;    // Note that with $a = 1, $a would remain an int (scalar) (and generate warnings)
while (true) {
    $a[1] = $a;
}

// The results depend on the parameter used in the array widening (mergeArrays)
// Parameter 3 : 
// Expected   [ $c => {TArray {TArray {TArray {TArray top}}}}
//              $a => {TArray {TNum, TArray {TNum, TArray {TNum, TArray top}}}} ] 
?>
