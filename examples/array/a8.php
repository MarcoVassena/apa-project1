<?php

if (TRUE) {
  $a[0] = 1;
} else {
  $a = True;
}

// $a here could be either TBool or TArray { TInt }

$b = $a[0];   // $b is set to top because since $a is possibly a scalar is the only sound choice.

?>
