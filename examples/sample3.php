<?php

// $x is null until the last iteration in which is int (0)

for ($x = NULL, $i = 10; $i >= 0; $i--){
  $x = $i ? $x : $i;
}
