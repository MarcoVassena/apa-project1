\documentclass[12pt, a4paper, oneside]{article}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{placeins}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[toc,page]{appendix}
\usepackage[T1]{fontenc}

% font size could be 10pt (default), 11pt or 12 pt
% paper size coulde be letterpaper (default), legalpaper, executivepaper,
% a4paper, a5paper or b5paper
% side coulde be oneside (default) or twoside 
% columns coulde be onecolumn (default) or twocolumn
% graphics coulde be final (default) or draft 
%
% titlepage coulde be notitlepage (default) or titlepage which 
% makes an extra page for title 
% 
% paper alignment coulde be portrait (default) or landscape 
%
% equations coulde be 
%   default number of the equation on the rigth and equation centered 
%   leqno number on the left and equation centered 
%   fleqn number on the rigth and  equation on the left side
%   



\title{APA Project 1 - PHP Soft Typing}
\author{Marco Vassena  \\
    4110161 \\
    \and
    Philipp Hausmann \\
    4003373 \\
    }

\date{\today}
\begin{document}



\maketitle

\tableofcontents

\section{Introduction}
This document contains the documentation of the first APA project. Please see
the file README.txt for build instructions.

\section{Supported PHP Features}
PHP is a big language with many features. Our soft typing analysis does not support all features
of current PHP versions. As it would be very verbose to list either all supported or unsupported
features, we will list just the support status of major features.	
Our implementation supports the following major PHP features:
\begin{itemize}
\item Functions
\item Imports
\item \texttt{eval} (without deep analysis)
\item Switch with break and default
\item Global and local variables
\item Arbitrary side-effects in expressions
\item Multidimensional heterogeneous arrays
\item \texttt{echo}
\end{itemize}

The support to global variables is somewhat limited.
The reason is explained in \ref{sec:function}.

The following PHP features are not supported:
\begin{itemize}
\item Classes
\item Nested/Conditional function definitions
\item \texttt{unset} Statement
\item \texttt{continue}/\texttt{break} for loops
\item \texttt{goto}
\item \texttt{Constants}
\item Static variables
\item References
\end{itemize}

We expect that some features like \texttt{constants}, \texttt{goto}, \texttt{continue} and  \texttt{break }could be implemented easily in the current implementation without major changes, unfortunately we did not have enough time to add them.
Other features like nested/conditional functions or classes are
likely to require more extensive modifications.


\section{Implementation}
The implementation is split into multiple parts.

The first part is the \texttt{Parser} module, which converts a file into an 
abstract syntax tree. This module is just a small wrapper around the
lex-pass \cite{LEX} library, to which the actual parsing is delegated.
The abstract syntax tree from the \texttt{Parser} module is then converted to
a small \texttt{Core} language defined in the module \texttt{Php}.

The general embellished monotone framework implementation can be found in
the \texttt{Monotone} module. The actual soft typing instance of this framework
is defined in the \texttt{SoftTyping} module.

Some additional code can be found in the \texttt{PrettyPrint} and \texttt{Main} modules,
but they are just a simple user interface not warranting special attention.

In the following sections some of the afore-mentioned modules will be described in more detail.

\subsection{Core Language}
The terms of the \texttt{Core} language can be seen in figure \ref{fig:core}. The \texttt{Core} language
is a small, imperative language defined in the module \texttt{Php.Core}. As the aim is to implement a soft typing analysis for PHP, the language
is tailored towards that goal. Due to that reason, only a subset of the information found in a regular
PHP program can be encoded in \texttt{Core}, but all information required for the soft typing
analysis is preserved. On the other hand, it is not possible to build a complete PHP interpreter
on top of the \texttt{Core} language used herein.

The semantics of the \texttt{Core} language should be mostly self-evident from the names of the
constructors. As a special case, the \texttt{Poison} statement indicates that any side effect might have
happened. The \texttt{Magic} expression, on the other hand, is an unknown value of any type.

Furthermore, the \texttt{Goto}/\texttt{JLabel} constructs are restricted to jumps in the same scope, jumping
outside or into functions is not allowed. Additionally, all labels are required to be unique
and all \texttt{goto}'s must point to a defined label.

The expressions inside statements are guaranteed to have no side effects, the only exception being
the right hand side of an assignment.

In the \texttt{Call} statement, \texttt{Var} is the target variable of the call, meaning the variable that will contain the result of calling the function.
The string \texttt{Identifier} is the name of the function being called and \texttt{[Expr]} is the list of arguments passed to the function.
The data type \texttt{FunDef} defines a new function.
It is defined as in \cite{NN99}.
The data type \texttt{Var} represents a variable.
The constructor \texttt{UserV} is used for user defined variables, whereas
\texttt{AuxV} is used for auxiliary variables introduced during the expression conversion process (see \ref{sec:flattening}).
\begin{figure}
\begin{lstlisting}
data Program = Program [FunDef] Stmt

data FunDef = FunDef Label Label Identifier Var [Var] Stmt

data Stmt =
    Ite     Label Expr Stmt Stmt
  | While   Label Expr Stmt
  | Seq     Stmt  Stmt
  | Asgn    Label Expr Expr
  | Skip    Label
  | Goto    Label JumpLabel
  | JLabel  Label JumpLabel
  | Poison  Label
  | Call    Label Label Var Identifier [Expr]

data Expr =
    LitString String
  | LitNum    String
  | LitBool   Bool
  | LitNull
  | Variable  Var
  | Array     Identifier [Expr]
  | BinExpr   BinOp Expr Expr
  | UnExpr    UnOp Expr
  | Magic
    
data Var =
    UserV Identifier
  | AuxV Int

\end{lstlisting}
\caption{Core Language. "Label" refers to the Monotone Framework labels, "JumpLabel" to labels relevant for the Goto/JLabel.}
\label{fig:core}
\end{figure}

\subsection{PHP to Core conversion}
The conversion from the PHP abstract syntax tree to the \texttt{Core} language is implemented
in the module \texttt{Php.Convert}.

\paragraph{Statement Conversion}
The conversion of the PHP statements is implemented in the module \texttt{Php.Convert.Stmt}.
For the most basic constructs such as if-then-else and while, the mapping from PHP to \texttt{Core} is straightforward
as they have a clear counterpart in \texttt{Core}.

For the switch statement, the conversion is more complicated due to the higher number of possible control flows.
The switch statement is converted to a if-then-else cascade, using \texttt{Goto}'s to match the evaluation semantics of PHP.

The \texttt{eval} statement is converted to a \texttt{Poison} statement, as it is impossible in general to
determine statically what effect an \texttt{eval} might have. The return value of an \texttt{eval} is \texttt{Magic},
for the same reason. This conversion is always sound, but can lead to imprecise results.
A more advanced implementation might try to inspect the argument of eval to arrive
at a better result.

The \texttt{include} statement of php disappears from \texttt{Core} altogether, as the included file
will be directly inlined during conversion. If the path of the \texttt{include} statement is not known statically, a \texttt{Poison} statement is inserted instead.

\paragraph{Expression Conversion} 
\label{sec:flattening}
The conversion of PHP expressions is defined in the module \texttt{Php.Convert.Flatten}.
It converts arbitrary expression into side-effect free expressions, where the side-effect
has been extracted and is returned separately. Let $pre$ be this side-effect. $pre$ is
a sequence of statements, which when evaluated causes the same side effects as the
original PHP expression. This statements will be inserted before the usage site of
the expression by the Statement Conversion described in the previous paragraph.

For expressions which are already side-effect free, this conversion returns the original
expression and $pre$ is just a \texttt{Skip} statement. For expressions with side-effects like
assignment or eval, auxiliary variables are introduced to store the result of the
evaluation and replace the original expression in their usage sites.

\subsection{Embellished Monotone Framework}
The module \texttt{Monotone} contains the implementation of the \emph{MFP} algorithm that performs a context sensitive, flow insensitive analysis and the definition of the relevant data structures.
We based our implementation on the algorithm presented in \cite{NN99} and
here we will describe only the relevant changes.
The algorithm (function \texttt{analyze}) and the \texttt{Monotone} data type are polymorphic over the lattice and the labels.
The context is hard-coded in the algorithm and partially hidden 
from the user. Specifically, the context is a call-string of labels representing the call sites. 
Although it could be abstracted and expressed as a (nested) lattice (see \cite{HE}), we decided not, in order to be able to
make the context limiting function parametric over the \texttt{Monotone} instance.
The algorithm, as it is, performs a forward analysis. It could however be easily extended to perform a backward analysis.
Since our task was to perform soft typing analysis for PHP we focused more on supporting the PHP language to a great extent
than creating a general Monotone Framework.

Instead of computing the inter-procedural flow ($IF$ in \cite{NN99}), we decided to annotate the inter-procedural edges with the data type \texttt{InterEdge}.
It stores the call and return labels ($l_c$ and $l_r$) of the call site and specifies the ``direction'' of the edge (\texttt{FunCall} or \texttt{FunRet}). 
These tags are inspected when an edge is popped from the working-list and used to modify the context accordingly.
If an \texttt{Inter} edge is tagged with \texttt{FunCall}, $l_c$ is pushed onto the context. If it is tagged with \texttt{FunRet}, the $l_c$ label is popped from those contexts that have a matching pending call from the call site $l_c$.
In addition, given the kind of the edge the arity
of the transfer function and what it arguments need to be can be inferred.
These tasks are carried out by the function \texttt{applyTransfer}.

Transfer functions are identified by a \texttt{TransferId}, which can be 
either \texttt{Simple}, \texttt{Call} or \texttt{Return}. 
The first variant is labeled with the source label of an intra-procedural edge.
The other two are labeled with the $l_c$ and $l_r$ of a call site.
When an edge is popped from the working list, based on its kind
a proper \texttt{TransferId} is built and used to lookup the correct transfer function that needs to be applied. Such a lookup function is provided by the user inside the \texttt{Monotone} instance (field \emph{transf}).

\subsection{Soft Typing Instance}
This section will describe the soft typing instance of the Monotone Framework. The
code can be found in the \texttt{SoftTyping} module.

\paragraph{Flow}
The flow is defined exactly as in \cite{NN99} and can be found in the
module \texttt{SoftTyping.Flow}.

\paragraph{Lattice}
\label{par:Lattice}
The lattice of choice for this analysis is a type map lattice, mapping variables to
their possible types. The possible types of a variable itself are a lattice as well,
which we will call the type lattice. The implementation resides in
the \texttt{SoftTyping.Lattice} module.

\subparagraph{Type Lattice}
The type lattice can be seen in figure \ref{fig:ltype}. 
In PHP uninitialized variables are set to NULL upon use. We have decided not to explicitly model these cases, therefore, regardless of which element in this lattice is chosen, null could actually be a possible value at run-time.
However if a variable is explicitly set to null, $Null$ will appear in the set of possible types. 

The $\sqcup$ operator basically is
set union, with a limit on the size of the set. If the set reaches the limit, the $\sqcup$ operator
returns $\top$. Additonally, arrays are handled differently as well. If more than one array type
would be in the joined result, the type inside the array is joined and a single Array-type retained.
The depth of Array-types is also limited, and $\top$ is used as array element type if the
maximum depth has been reached.

With the restriction on maximal recursion level of arrays, the set of possible types is finite.
The definition of $\top$ is given as well, the lattice is therefore complete.

\begin{figure}
\begin{align*}
\tau &= Bool | String | Num | Null | Array LType \\
LType &= AnyOf \; \{ \tau \} \; | \; Top \\
\top &= Top \\
\bot &= AnyOf \; \emptyset \\
\_ \sqsubseteq Top &= True \\
Top \sqsubseteq (AnyOf \; \_) &= False \\
(AnyOf \; x) \sqsubseteq (AnyOf \; y) &= x \subseteq y \\ 
\end{align*}
\caption{Type lattice definition. $\tau$ is the representation of PHP types.}
\label{fig:ltype}
\end{figure}

\subparagraph{Type Map Lattice}
The type map lattice contains a mapping from variables to type lattice elements.
The formal definition can be seen in \ref{fig:ltmap}.
In addition, it also contains a default value which applies to all variables
not explicitly mentioned in the mapping. This default value is used
to implement the Poison statement of the \texttt{Core} language, because the set of defined variables is not know thereafter. No widening is applied, but could be
implemented in the future using the default value. A canonical representation
of the lattice elements is used, where all mappings with the same value
as the default value are removed. Semantical equivalence therefore equals
syntactical equivalence.
Because the set of statically known variables is finite, this lattice is finite as well. A $\bot$
element is defined too, the lattice is hence complete.

For the $\sqsubseteq$ operator, various things need to be compared. The default of the first value
needs to be under or equal to the default of the second argument. For all entries inside the mappings,
it has to be verified that the associated \texttt{LType} value in the first argument is under the corresponding
mapping in the second argument. This comparison is required to respect the default values of both
arguments.

The $\sqcup$ operator is defined very similar to the $\sqsubseteq$ operator, in that the default
values and all mappings are joined. Once again, the default values need to be considered
for mappings only occurring in one argument.
\begin{figure}
\begin{align*}
LTypeMap &= (LType, {Var \to LType}) \\
\top &= (\top, \emptyset) \\
\bot &= (\bot, \emptyset) \\
\end{align*}
\caption{Type Map lattice definition.}
\label{fig:ltmap}
\end{figure}

\paragraph{Context limiting function}
Call strings of bounded length are used as context in the analysis.
	\[ \Delta = \texttt{Label}^{\leq k} \]
The parameter $k$ can be specified from the command line with the option \texttt{-k}.


\paragraph{Transfer Function}
The transfer function is defined in the module \texttt{SoftTyping.Transfer}. 

For most
statements the transfer function is just the identity function, as no new typing
information arises. We will only discuss the cases where the transfer function differs.

\subparagraph{Assignment}
The transfer function for assignments always updates the type mapping of the left-hand side variable
with the possible types of the right hand side. If the right hand side is a literal, the
type of the literal is used. If the right hand side is a variable, the possible types are looked up in the
type mapping.

For more complicated expressions like unary or binary operators, the arguments are inspected
and the resulting types are computed according to the PHP semantics. Additionally, for
unary operators with side effects such as increment (++) or decrement (\--\--) the
type of the modified variable is updated as well.


\subparagraph{Call} 
A \texttt{Call} statement generates three transfer functions.
A \texttt{Call} transfer function which produces a new lattice containing only the formal parameters of the function and the global variables used by the function being called, both initialized with their current types in the caller lattice.
A \texttt{Return} \texttt{Binary} transfer function that combines the caller and the function lattices.
It returns the caller lattice, with the target variable set to the return variable types (from the function lattice) and the global variables set to their current type (from the function lattice).
Note that they are set, rather than joined with the caller lattice.
A \texttt{Simple} transfer function labeled with $l_r$, which is just the identity function, as after returning from the function no other semantics effects take place.


\subsection{Global variables and Functions}
\label{sec:function}
The module \texttt{SoftTyping.Function} computes and collects information about the functions defined in a program, which are used mainly in the \texttt{SoftTyping.Flow} and \texttt{SoftTyping.Transfer} functions.

In PHP global variables are not automatically available inside a function.
In order to refer to a global variable the keyword \texttt{global} must be used.
In order to support global variables in our analysis these annotations are collected and provided to the transfer function, so that their current type can flow from the caller to the function body.
At the moment only global variables directly accessible by a function are considered and threaded. However a function can possibly call another function that uses different global variables, which would not be available leading to wrong results.
In order to deal with this problem properly we should find the cliques of mutually recursive functions. Once these are found the set of global variables that needs to be passed to each is the union of the global variables used directly by each. In this way each function would receive the correct set of global variables actually needed to perform a sound analysis.

\section{Examples}
In the following sections we are going to discuss two example analysis. We would like
to mention that redundant information like additional \texttt{skip} statements have been
omitted in the output below.

The full set of examples can be found in the "examples" directory with the full output
of all analysis in the corresponding "*.log" files.
\subsection{Include}
\begin{figure}
\begin{subfigure}[b]{0.5\textwidth}
\lstinputlisting[language=PHP]{../examples/report/1_main_include.php}
\caption{1\_main\_include.php}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
\lstinputlisting[language=PHP]{../examples/report/1_aux_a.php}
\caption{1\_aux\_a.php}
\end{subfigure}

\begin{subfigure}[b]{0.5\textwidth}
\lstinputlisting[language=PHP]{../examples/report/1_aux_b.php}
\caption{1\_aux\_b.php}
\end{subfigure}
\caption{PHP source of include example.}
\label{src:php-ex1}
\end{figure}

\begin{figure}
\begin{subfigure}[b]{\textwidth}
\begin{lstlisting}
user_a <- "bla" [0]
user_b <- 234.2 [2]
user_file_b <- "1_aux_b.php" [7]
if (LitBool True) [10]{
  poison  [11]
  user_b <- (user_b) + (12) [13]
  skip  [14]
} else {
  user_b <- "blub" [16]
  skip  [17]
}
aux_0 <- UnExpr PostIncr (Variable user_b) [19]
aux_1 <- (aux_0) + (12) [20]
user_a <- aux_1 [21]
skip  [22]
\end{lstlisting}
\caption{Core code.}
\label{src:ex1-core}
\end{subfigure}

\begin{subfigure}[b]{\textwidth}
\begin{lstlisting}[basicstyle=\footnotesize,breaklines=true]
 [0] -> [* => {}, user_a => {TString}]
 [2] -> [* => {}, user_a => {TString}, user_b => {TNum}]
 [7] -> [* => {}, user_a => {TString}, user_b => {TNum}, user_file_b => {TString}]
 [10] -> [* => {}, user_a => {TString}, user_b => {TNum}, user_file_b => {TString}]
 [11] -> [* => top]
 [13] -> [* => top]
 [14] -> [* => top, user_b => {TNum}]
 [16] -> [* => {}, user_a => {TString}, user_b => {TString, TNum}, user_file_b => {TString}]
 [17] -> [* => {}, user_a => {TString}, user_b => {TString}, user_file_b => {TString}]
 [19] -> [* => top, user_b => {TString, TNum}]
 [20] -> [* => top, user_b => {TString, TNum}, aux_0 => {TString, TNum}]
 [21] -> [* => top, user_b => {TString, TNum}, aux_0 => {TString, TNum}, aux_1 => {TNum}]
 [22] -> [* => top, user_a => {TNum}, user_b => {TString, TNum}, aux_0 => {TString, TNum}, aux_1 => {TNum}]
\end{lstlisting}
\caption{Soft typing result.}
\label{soft:ex1}
\end{subfigure}
\caption{The result of the analysis of the include example.}
\end{figure}

The first example shows how the soft typing analysis performs for intra-procedural
code. The PHP code listed in \ref{src:php-ex1} is converted into the single \texttt{Core}
fragment shown in \ref{src:ex1-core}.
The "<-" arrow indicates assignment, and the numbers in brackets "[x]" designate
the labels used in the Monotone algorithm. The first PHP include is directly inlined.
The second include at label [11] cannot be statically inlined, because
the file is not statically known. This leads to the introduction of the \texttt{poison} statement
on that line. The assignment with the post-increment on the right hand side is
flattened into multiple statement with auxiliary variables at labels [19] - [22].

The result of the soft-typing analysis can be seen in \ref{soft:ex1}.
The first number is the label, the second part is
the lattice element for that label. The lattice element can be viewed as
association list. For variables not explicitly mentioned, the "*" entry
indicates the appropriate value (= default value).
For the directly included file, the results are as expected with the types for
\texttt{user\_a} and \texttt{user\_b} being determined by the literals. More interestingly, the
poison statement causes the default value in the lattice going to top and
the loss of all type information,
as it is not known what might happened. At label [13], it can be seen
that assigning to a variable after a poison statement introduces knowledge
about it's type anew.
At label [19], the information from the two branches of the if gets
joined and \texttt{user\_b} now can have type TString or TNum. Because the
variable \texttt{user\_a} has type top in the first branch, the join is top
as well and it disappears from the type mapping due to the normalization of the mapping as described in \ref{par:Lattice}.

\subsection{Functions}
This example demonstrates how functions influence the results, and how context
helps improving the precision.

The PHP code of this example can be seen in \ref{src:ex2}.
\begin{figure}
\lstinputlisting[language=PHP]{../examples/report/2_main_func_poison.php}
\caption{PHP source of function example.}
\label{src:ex2}
\end{figure}
\begin{figure}
\begin{lstlisting}
fun id(user_x) aux_0 [0]
{
  aux_0 <- user_x [2]
} [4]
aux_1 <- id(12) [6] [7]
user_a <- aux_1 [8]
aux_2 <- id("test") [10] [11]
user_b <- aux_2 [12]
aux_3 <- id(LitNull) [14] [15]
user_c <- aux_3 [16]
\end{lstlisting}
\caption{Core code of function example.}
\label{src:ex2-core}
\end{figure}
\begin{figure}
\begin{lstlisting}[basicstyle=\footnotesize,breaklines=true]
 [0] -> [* => {}, user_x => {TString, TNum, TNull}]
 [2] -> [* => {}, user_x => {TString, TNum, TNull}, aux_0 => {TString, TNum, TNull}]
 [4] -> [* => {}, user_a => {TString, TNum, TNull}, user_b => {TString, TNum, TNull}, user_x => {TString, TNum, TNull}, aux_0 => {TString, TNum, TNull}, aux_1 => {TString, TNum, TNull}, aux_2 => {TString, TNum, TNull}, aux_3 => {TString, TNum, TNull}]
 [6] -> [* => {}, user_x => {TNum}]
 [7] -> [* => {}, aux_1 => {TString, TNum, TNull}]
 [8] -> [* => {}, user_a => {TString, TNum, TNull}, aux_1 => {TString, TNum, TNull}]
 [10] -> [* => {}, user_a => {TString, TNum, TNull}, user_x => {TString}, aux_1 => {TString, TNum, TNull}]
 [11] -> [* => {}, user_a => {TString, TNum, TNull}, aux_1 => {TString, TNum, TNull}, aux_2 => {TString, TNum, TNull}]
 [12] -> [* => {}, user_a => {TString, TNum, TNull}, user_b => {TString, TNum, TNull}, aux_1 => {TString, TNum, TNull}, aux_2 => {TString, TNum, TNull}]
 [14] -> [* => {}, user_a => {TString, TNum, TNull}, user_b => {TString, TNum, TNull}, user_x => {TNull}, aux_1 => {TString, TNum, TNull}, aux_2 => {TString, TNum, TNull}]
 [15] -> [* => {}, user_a => {TString, TNum, TNull}, user_b => {TString, TNum, TNull}, aux_1 => {TString, TNum, TNull}, aux_2 => {TString, TNum, TNull}, aux_3 => {TString, TNum, TNull}]
 [16] -> [* => {}, user_a => {TString, TNum, TNull}, user_b => {TString, TNum, TNull}, user_c => {TString, TNum, TNull}, aux_1 => {TString, TNum, TNull}, aux_2 => {TString, TNum, TNull}, aux_3 => {TString, TNum, TNull}]
\end{lstlisting}
\caption{Soft typing result with $k$ $0$ for the function example.}
\label{soft:ex2-k0}
\end{figure}
\begin{figure}
\begin{lstlisting}[basicstyle=\footnotesize,breaklines=true]
 [0] -> [* => {}, user_x => {TString, TNum, TNull}]
 [2] -> [* => {}, user_x => {TString, TNum, TNull}, aux_0 => {TString, TNum, TNull}]
 [4] -> [* => {}, user_a => {TNum}, user_b => {TString}, user_x => {TString, TNum, TNull}, aux_0 => {TString, TNum, TNull}, aux_1 => {TNum}, aux_2 => {TString}, aux_3 => {TNull}]
 [6] -> [* => {}, user_x => {TNum}]
 [7] -> [* => {}, aux_1 => {TNum}]
 [8] -> [* => {}, user_a => {TNum}, aux_1 => {TNum}]
 [10] -> [* => {}, user_a => {TNum}, user_x => {TString}, aux_1 => {TNum}]
 [11] -> [* => {}, user_a => {TNum}, aux_1 => {TNum}, aux_2 => {TString}]
 [12] -> [* => {}, user_a => {TNum}, user_b => {TString}, aux_1 => {TNum}, aux_2 => {TString}]
 [14] -> [* => {}, user_a => {TNum}, user_b => {TString}, user_x => {TNull}, aux_1 => {TNum}, aux_2 => {TString}]
 [15] -> [* => {}, user_a => {TNum}, user_b => {TString}, aux_1 => {TNum}, aux_2 => {TString}, aux_3 => {TNull}]
 [16] -> [* => {}, user_a => {TNum}, user_b => {TString}, user_c => {TNull}, aux_1 => {TNum}, aux_2 => {TString}, aux_3 => {TNull}]
\end{lstlisting}
\caption{Soft typing result with $k$ $1$ for the function example.}
\label{soft:ex2-k1}
\end{figure}

The conversion to \texttt{Core} doesn't yield any surprises and can be seen in \ref{src:ex2-core}.

The maximal call string length used during the analysis can be set using the
parameter "-k". Valid values are $0$ or greater.
If executed with a $k$ of $0$, poisoning can be observed in \ref{soft:ex2-k0}. The
results of the different function call is the join over all calls and imprecise.

If executed with a $k$ of $1$, the poisoning disappears as can be seen in \ref{soft:ex2-k1}.

\subsection{Array}
The example ``3\_array\_widening.php'' shows the widening on the nested array taking place. Without it the analysis would endlessly loop trying to compute the type of \texttt{\$a} after the loop, joining the initial type with 
its type nested recursively inside another array.
The type computed with widening depends on the parameter used.
The information about the array is preserved until the widening bound is hit
(in this case at the 4th nesting), setting the type within the \texttt{TArray} to \texttt{top}.

\section{Conclusion}
We have shown one way of defining a soft typing analysis for PHP and how it can be implemented. Our implementation
supports a fair part of the PHP semantics, especially if one restricts the input to well-coded programs following
established coding practices.

Our implementation achieves a good level of precision with the usage of context. To some degree, this achievement
is countered by the complicated semantics of PHP which introduce a fair amount of imprecision. The creative PHP semantics
are likely to cause problems for other implementations and analysis as well. We expect it to be costly in terms of running time or
coding effort to avoid the imprecision penalty caused by the PHP semantics.

We consider the presented solution
a successful outcome of this project and view it as a good foundation for further research in this area.

\begin{thebibliography}{3}

\bibitem{NN99}
  Nielson, F. and Nielson, H.R. and Hankin, C.
  \emph{Principles of Program Analysis}

\bibitem{HE}
	\url{https://github.com/henkerik/typing}

\bibitem{LEX}
	\url{https://github.com/phile314/lex-pass}

\end{thebibliography}



\end{document}
