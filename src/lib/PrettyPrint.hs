{-# LANGUAGE FlexibleInstances #-}

module PrettyPrint (
    pprint
  , PPrint
  , PPrintWith
  , pprintWith
  ) where

import PrettyPrint.Simple
import PrettyPrint.Verbose

