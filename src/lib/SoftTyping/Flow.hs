module SoftTyping.Flow (mkFlow, mkFTable, FunTable, FunInfo(..)) where

import Data.Set hiding (map)
import qualified Data.Map as M
import Control.Monad.Reader

import Monotone (Edge(..), InterEdge(..) )
import Php
import SoftTyping.Function

-- Computes the control flow graph of the given program, i.e.
-- the edges connecting labels of statments in the program that
-- represent possible executions in the program.
mkFlow :: Program -> Set (Edge Label)
mkFlow p@(Program fs s) = runReader (flow p) flowS
  where flowS = FlowS { jTable = M.unions $ [mkJTable s] ++ map (mkJTable . body) fs,
                        fTable = mkFTable p}

--------------------------------------------------------------------------------
-- JumpTable
--------------------------------------------------------------------------------

-- | Specifies at what 'Stmt' label corresponds each
-- 'JLabel' present in a program
type JumpTable = M.Map JumpLabel Label

mkJTable :: Stmt -> JumpTable
mkJTable (Ite _ _ b1 b2) = M.unions [mkJTable b1, mkJTable b2]
mkJTable (While _ _ b) = mkJTable b
mkJTable (JLabel lbl jlbl) = M.singleton jlbl lbl
mkJTable (Seq s1 s2) = mkJTable s1 `M.union` mkJTable s2
mkJTable _ = M.empty

--------------------------------------------------------------------------------
-- FlowM monad
--------------------------------------------------------------------------------

data FlowS = FlowS { jTable :: JumpTable,
                     fTable :: FunTable }

type FlowM = Reader FlowS

-- | Returns the initial label and final label associated with the definition 
-- of the given function.
-- Fails with error if the function has no definition.
getFunLabels :: Identifier -> FlowM (Label, Label)
getFunLabels name = do
  funTable <- asks fTable
  case M.lookup name funTable of
    Just r -> return (labelBegin r, labelEnd r)
    Nothing -> error $ "getFunLabels: Undefined function " ++ name

-- | Returns the 'Label' corresponding to the given 'JumpLabel'.
-- Fails if the given 'JumpLabel' is invalid (neved defined by a JLabel in the program)
getJumpToLabel :: JumpLabel -> FlowM Label
getJumpToLabel jlbl = do
  r <- asks (M.lookup jlbl . jTable)
  case r of
    Just l -> return l
    Nothing -> error $ "getJumpToLabel: Undefined JumpLabel " ++ show jlbl

class HasLabel a => Flowable a where
  -- | Computes the edges of the control flow graph.
  flow :: a -> FlowM (Set (Edge Label))

instance Flowable Program where
  flow (Program fs b) = do
    bodyFlow <- flow b
    funsFlow <- mapM flow fs >>= return . unions
    return $ funsFlow `union` bodyFlow

instance Flowable Stmt where
  flow (Ite lbl e b1 b2) = do
         f1 <- flow b1
         f2 <- flow b2
         return $ f1 `union` f2 `union` stepIn
     where stepIn = fromList [Step lbl (initLabel b1) , Step lbl (initLabel b2)]
  
  flow (While lbl e b) = do
        fb <- flow b
        return $ unions [fb, stepIn, stepBack]
    where stepIn = singleton (Step lbl (initLabel b))
          stepBack = fromList [Step l lbl | l <- final b]
  
  flow (Asgn lbl e1 e2) = return empty
  flow (Skip lbl) = return empty
  flow (Goto from jlbl) = do
    to <- getJumpToLabel jlbl
    return $ singleton (Step from to)
  flow (JLabel lbl jlbl) = return empty
  flow (Poison lbl) = return empty
  flow (Global l ss) = return empty
  flow (Call lc lr res fun exprs) = do
         (ln, lx) <- getFunLabels fun
         return $ fromList [Inter lc ln (FunCall lc lr), Inter lx lr (FunRet lc lr)]
  flow (Seq s1 s2) = do
    f1 <- flow s1
    f2 <- flow s2
    return $ f1 `union` f2 `union` link
    where link = fromList [ Step l1 l2 | l1 <- final s1 ]
          l2 = initLabel s2

instance Flowable FunDef where
  flow (FunDef ln lx fun res parms b) = do
        fb <- flow b
        return $ fb `union` stepIn `union` stepOut
    where stepIn = singleton $ Step ln (initLabel b)
          stepOut = fromList [ Step l lx |  l <- final b ]

