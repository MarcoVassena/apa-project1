{-# LANGUAGE FlexibleInstances #-}
module SoftTyping.Lattice
  ( LTypeMap
  , getDefault
  , getMapping
  , mkTypeMap
  , setType
  , setTypes
  , lookupTy
  , showEntry
  , showDefault
  , LType (..)
  , Type (..)
  , isArray
  , isScalar
  , one
  , array
  , anyOf
  )  where

import Data.Map (Map, foldWithKey)
import qualified Data.Map as M
import Data.Set (Set)
import qualified Data.Set as S
import Lattice
import Php.Core (Var(..), Expr(..))
import Data.List (intercalate)
import Data.Maybe (fromMaybe)

--------------------------------------------------------------------------------
-- LTypeMap
--------------------------------------------------------------------------------

data LTypeMap
  --       default   mapping
  = LTypeMap LType (Map Var LType)
  -- we can use derived eq because our join always returns the smallest possible value in a canonical form.
  deriving Eq

instance Lattice LTypeMap where
  top = LTypeMap top M.empty
  bottom = LTypeMap bottom M.empty

  join (LTypeMap d1 m1) (LTypeMap d2 m2) =
    normalize $ LTypeMap d' m
    where d' = d1 `join` d2
          -- we have to handle the default values for keys appearing in only one of the mappings
          i   = M.intersectionWith join m1 m2
          m1' = M.map (join d2) (m1 `M.difference` i)
          m2' = M.map (join d1) (m2 `M.difference` i)
          m   = i `M.union` m1' `M.union` m2'


  -- | <=-operator for type map lattices.
  underOrEqual (LTypeMap d1 m1) (LTypeMap d2 m2) =
    (d1 `underOrEqual` d2) && iOk && m1Ok && m2Ok
    where -- we have to handle the default values for keys appearing in only one of the mappings
          i    = M.intersectionWith underOrEqual m1 m2
          iOk  = and $ M.elems i
          m1Ok = and $ M.elems $ M.map (\x -> underOrEqual x d2) (m1 `M.difference` i)
          m2Ok = and $ M.elems $ M.map (\x -> underOrEqual d1 x) (m2 `M.difference` i)

-- remove all mappings to the default value, as they are redundant.
normalize :: LTypeMap -> LTypeMap
normalize (LTypeMap d m) = LTypeMap d $ M.filter (/=d) m

mkTypeMap :: LType -> Map Var LType -> LTypeMap
mkTypeMap d m = normalize $ LTypeMap d m

getDefault :: LTypeMap -> LType
getDefault (LTypeMap d _) = d

getMapping :: LTypeMap -> Map Var LType
getMapping (LTypeMap _ m) = m

lookupTy :: LTypeMap -> Var -> LType
lookupTy (LTypeMap d m) v = fromMaybe d (v `M.lookup` m) -- fall back to default

class Typeable a where
  setType :: a -> LType -> LTypeMap -> LTypeMap

-- TODO this is rather dangerous ... fix it
instance Typeable Expr where
  setType (Variable v) lt ltm = setType v lt ltm
  -- Arrays are only user defined variables
  setType (Array name es) lt ltm = setType (UserV name) lt ltm 
  setType e _ _ = error $ "setType: This expression is not tracked" ++ show e

instance Typeable Var where
  setType v lt (LTypeMap d m) = mkTypeMap d $ M.insert v lt m

setTypes :: [(Var,LType)] -> LTypeMap -> LTypeMap
setTypes xs (LTypeMap d m) = mkTypeMap d $ foldr insert m xs
  where insert (k,v) = M.insert k v

--------------------------------------------------------------------------------
-- LType
--------------------------------------------------------------------------------

data LType
  = AnyOf (Set Type)
  | Top
  deriving (Ord, Eq)

-- The types available in Php.
data Type = TBool 
          | TString
          | TNum     -- ^ Both for floats and ints
          | TNull
          | TArray LType   -- ^ At the moment we consider only homogeneous arrays
  deriving (Eq, Ord, Show)

instance Lattice LType where
  top = Top
  bottom = AnyOf S.empty  -- empty set indicates unitialized variables.
  join (AnyOf s1) (AnyOf s2) = if S.size s > 4 then Top else AnyOf normalized
    where s = s1 `S.union` s2
          normalized = mergeArrays s
  join _ _ = Top

  underOrEqual _          Top        = True
  underOrEqual Top        (AnyOf _)  = False
  underOrEqual (AnyOf s1) (AnyOf s2) = s1 `S.isSubsetOf` s2

--------------------------------------------------------------------------------
-- Helper functions
--------------------------------------------------------------------------------

isArray :: Type -> Bool
isArray (TArray _) = True
isArray _ = False 

isScalar :: Type -> Bool
isScalar = not . isArray

-- | Returns a set in which there is at most one 'TArray'.
-- Its type is computed merging recursively all the instances
-- of arrays found.
-- Widening is also applied so that arrays with more than 3 dimension
-- are restricted to 'Top'.
-- e.g. $a[0][0][0][0] : $a => {TArray {TArray {TArray {TArray top}}}}
mergeArrays :: Set Type -> Set Type
mergeArrays s = if S.null arrays then s else (others `S.union` joinedArray)
  where (arrays, others) = S.partition isArray s
        joinedArray = S.singleton (TArray mergedTy)
        mergedTy = limit 3 $ joinLists [lt | TArray lt <- S.toList arrays]

-- | Limits the arbitrary nesting in arrays.
-- When the maximum number of recursive 'TArray' is encountered,
-- the dangling 'LType' is set to 'Top'
limit :: Int -> LType -> LType
limit n Top = Top
limit n (AnyOf s) = AnyOf $ S.map (f n) s
  where f 1 (TArray lt) = TArray Top
        f n (TArray lt) = TArray (limit (n-1) lt)
        f _ ty = ty

--------------------------------------------------------------------------------
-- | Shorthands
--------------------------------------------------------------------------------

-- | Lifts the given 'LType' into an array type.
-- > array 
array :: LType -> LType
array = one . TArray

-- | Lifts a 'Type' into an 'LType'
one :: Type -> LType
one = AnyOf . S.singleton

-- | Lifts a list of 'Type's into an 'LType'
anyOf :: [Type] -> LType
anyOf = AnyOf . S.fromList

--------------------------------------------------------------------------------
-- Show instances and functions
--------------------------------------------------------------------------------

instance Show LTypeMap where
  show (LTypeMap d m) = "[" ++ intercalate ", " (d' : ms) ++ "]"
    where d' = showDefault d
          ms = map (uncurry showEntry) $ M.toList m

showEntry :: Var -> LType -> String
showEntry v t = show v ++ " => " ++ show t

showDefault :: LType -> String
showDefault d = "* => " ++ show d 

instance Show LType where
  show (Top)     = "top"
  show (AnyOf s) = "{" ++ intercalate ", " (map show $ S.toList s) ++ "}"

