module SoftTyping.Function where

import qualified Data.Map as M
import qualified Data.Set as S
import Php 

--------------------------------------------------------------------------------
-- FunTable
--------------------------------------------------------------------------------

-- The prototype of a function plus additional inforamation.
data FunInfo = FunInfo { labelBegin :: Label,  -- ^ Label at the definition
                         labelEnd :: Label,    -- ^ Label after the body
                         returnVar :: Var,     -- ^ Variable where the result of the function is stored
                         params :: [Var],       -- ^ Formal parameters
                         globalVars :: S.Set Var -- ^ Global variables used in the function
                       }

-- For each function defined store useful information ('FunInfo').
type FunTable = M.Map Identifier FunInfo

mkFTable :: Program -> FunTable
mkFTable (Program fs b) = M.fromList $ map build fs
  where build (FunDef l1 l2 name res params b) = (name, FunInfo l1 l2 res params (globals b))
        
-- Collects the global variables declared in a 'Stmt'
globals :: Stmt -> S.Set Var
globals (Ite _ _ s1 s2) = globals s1 `S.union` globals s2
globals (While _ _ s) = globals s
globals (Seq s1 s2) = globals s1 `S.union` globals s2
globals (Global _ vars) = S.fromList $ map UserV vars
globals _ = S.empty
