module SoftTyping.Transfer
  ( TTable
  , mkTransfer
  , buildTTable
  ) where

import Lattice
import Monotone hiding (Call)
import qualified Monotone as Mono (TransferId( Call ))
import Php.Core
import SoftTyping.Lattice
import SoftTyping.Flow

import Prelude hiding (concatMap)
import qualified Data.List as L
import qualified Data.Map as M
import qualified Data.Set as S 
import Data.Maybe (fromMaybe)
import qualified Data.Foldable as F

type TTable = M.Map (TransferId Label) (Transfer LTypeMap)

mkTransfer :: Program -> (TransferId Label) -> Transfer LTypeMap
mkTransfer p l = fromMaybe errMsg $ l `M.lookup` (buildTTable p)
  where errMsg = error $ "Transfer function for label " ++ show l ++ " not defined."

buildTTable :: Program -> TTable
buildTTable p@(Program fs b) = fsTT `M.union` bTT
  where fTable = mkFTable p
        fsTT = M.unions $ map (visit fTable) fs
        bTT = visit fTable b

insertSimple :: Label -> a -> M.Map (TransferId Label) a -> M.Map (TransferId Label) a
insertSimple l = M.insert (Simple l)

singleSimple :: Label -> a -> M.Map (TransferId Label) a
singleSimple l = M.singleton (Simple l)

-- Shorthand
tId :: Transfer a
tId = Unary id

--------------------------------------------------------------------------------
-- Transfer Functions
--------------------------------------------------------------------------------

class Visitable a where
  -- | Builds a transfer function table ('TTable')
  visit :: FunTable -> a -> TTable

instance Visitable Stmt where

  visit ft (Ite l e s1 s2)   = insertSimple l tId (visit ft s1 `M.union` visit ft s2)
  visit ft (While l e b)     = insertSimple l tId (visit ft b)
  
  visit _ (Asgn l (Array name args) e) =
    singleSimple l $ Unary $ \a ->
      let eTy = typeOf a e
          nameTy = lookupTy a (UserV name)
          nameNewTy = typeOfArray nameTy (length args) eTy in
            setType (UserV name) (nameTy `join` nameNewTy) a

  visit _ (Asgn l v e2@(UnExpr uop e)) | isPostOp uop =
      singleSimple l (Unary $ \a ->
        let e2ty = typeOfUnarySideEffect a uop e
            a2 = setType e e2ty a in
              setType v (typeOf a e2) a2 )
  
  visit _ (Asgn l v e2@(UnExpr uop e)) | isPreOp uop =
      singleSimple l (Unary $ \a -> 
        let e2ty = typeOfUnarySideEffect a uop e
            a2 = setType e e2ty a in
             setType v (typeOf a2 e2) a2 )

  visit _ (Asgn l v e2)       = singleSimple l (Unary $ \a -> setType v (typeOf a e2) a)
  visit _ (Skip l)            = singleSimple l tId
  visit _ (Goto l jl)         = singleSimple l tId
  visit _ (JLabel l jl)       = singleSimple l tId
  visit _ (Global l _)        = singleSimple l tId
  visit _ (Poison l)          = singleSimple l (Unary $ const top)
  visit ft (Seq s1 s2) = visit ft s1 `M.union` visit ft s2

  visit ft c@(Call lc lr res fun args) = M.unions [m1, m2, m3]
    where m1 = M.singleton (Mono.Call lc lr) $  Unary (setCall fDef args)
          m2 = M.singleton (Return lc lr) $ Binary (setReturn fDef res)
          m3 = singleSimple lr tId
          fDef = ft M.! fun       -- if undefined we would have already crashed in flow
 

instance Visitable FunDef where
  visit ft (FunDef lc lr fun _ params b) = M.unions [entryT, exitT, bodyT]
    where entryT = singleSimple lc tId
          exitT = singleSimple lr tId
          bodyT = visit ft b

-- Binary Transfer function for returning from the function, whose 'FunInfo' is given.
-- Sets in the call environment 
--  1) The result variable to the resulting type from return environment.
--  2) The global variables to their current type from the return environment.
setReturn :: FunInfo -> Var -> LTypeMap -> LTypeMap -> LTypeMap
setReturn fDef res alc alr = setTypes [(v, lookupTy alr v) | v <- gVars] (setType res retTy alc)
  where gVars = S.toList (globalVars fDef)
        retTy = lookupTy alr (returnVar fDef)
        
-- Transfer function for calling a function.
-- It sets the formal parameters to the type of the given arguments and
-- adds the global variables used by the called function to the current type
setCall :: FunInfo -> [Expr] -> LTypeMap -> LTypeMap
setCall fDef args alc = mkTypeMap (getDefault alc) (setParams `M.union` setGlobals)
  where setParams = M.unions $ zipWith single ps args
        single p e = M.singleton p (typeOf alc e)
        ps = params fDef
        setGlobals = M.fromList [ (gVar, lookupTy alc gVar) | gVar <- gVars]
        gVars = S.toList (globalVars fDef)

--------------------------------------------------------------------------------
-- Functions that compute set of types
--------------------------------------------------------------------------------

typeOf :: LTypeMap -> Expr -> LType
typeOf _ (LitString _)  = one TString
typeOf _ (LitNum _)     = one TNum
typeOf _ (LitBool _)    = one TBool
typeOf _ LitNull        = one TNull
typeOf l (Variable v)   = lookupTy l v
-- Constants??

-- TODO when we will add arrays we have to check the type 
-- of the expressions, as they would lead to a crash
typeOf _ (BinExpr BConcat e1 e2) = one TString
typeOf _ (BinExpr BMod e1 e2) = anyOf [TNum, TBool]
typeOf _ (BinExpr BDiv e1 e2) = anyOf [TNum, TBool]
typeOf _ (BinExpr bop e1 e2) | isArithmetic bop = one TNum
typeOf _ (BinExpr bop e1 e2) | isComparison bop = one TBool
typeOf _ (BinExpr bop e1 e2) | isLogical bop = one TBool
typeOf l (UnExpr uop e) = typeOfUnaryExpr l uop e
typeOf l (Array name args) = typeOfArrayElem (typeOf l userV) (length args)
  where userV = Variable (UserV name)
-- Ref
-- Include
typeOf _ Magic          = top
typeOf _ s              = error $ "typeOf for statement " ++ show s ++ " missing."

-- Returns the resulting types of applying a unary operator.
-- The first element of the tuple are the possible types of the expression itself.
-- The second element of the tuple are the possible types resulting from side effects.
-- This happens when ++ and -- are involved.
unaryLogic :: UnOp -> Type -> (S.Set Type, S.Set Type)
unaryLogic UNot ty = (S.singleton TBool, S.singleton ty)

unaryLogic PostIncr TNull = (S.singleton TNull, S.singleton TNum)
unaryLogic PostIncr TString = (S.singleton TString, S.fromList [TString, TNum])
unaryLogic PostIncr ty = (S.singleton ty, S.singleton ty)

unaryLogic PreIncr TNull = (S.singleton TNum, S.singleton TNum)
unaryLogic PreIncr TString = (S.fromList [TString, TNum], S.fromList [TString, TNum])
unaryLogic PreIncr ty = (S.singleton ty, S.singleton ty)

unaryLogic PreDecr TString = (S.fromList [TString, TNum], S.fromList [TString, TNum])
unaryLogic PreDecr ty = (S.singleton ty, S.singleton ty)

unaryLogic PostDecr TString = (S.singleton TString, S.fromList [TString, TNum])
unaryLogic PostDecr ty = (S.singleton ty, S.singleton ty)

typeOfUnary :: LTypeMap -> UnOp -> Expr -> (LType, LType)
typeOfUnary l UNot e = (one TBool, typeOf l e)
typeOfUnary l uop e = 
  case typeOf l e of
    Top -> (Top, Top)
    AnyOf tys -> let (exprTy, sideTy) = unzip $ map (unaryLogic uop) (S.toList tys) in
                      (AnyOf $ S.unions exprTy, AnyOf $ S.unions sideTy)

typeOfUnaryExpr :: LTypeMap -> UnOp -> Expr -> LType
typeOfUnaryExpr l uop = fst . typeOfUnary l uop

typeOfUnarySideEffect :: LTypeMap -> UnOp -> Expr -> LType
typeOfUnarySideEffect l uop = snd . typeOfUnary l uop

-- | Computes the type of an element of an array at a certain dimension, given
-- the type of the array.
typeOfArrayElem :: LType    -- ^ The type of the array
                -> Int      -- ^ The dimension accessed (e.g. a[0] - dim 1, a[0][0] dim 2 ...)
                -> LType
typeOfArrayElem Top _ = Top
typeOfArrayElem (AnyOf s) n = joinLists $ map (inspect n) (S.toList s)
  where inspect :: Int -> Type -> LType
        inspect 0 ty = one ty
        inspect n (TArray eTy) = typeOfArrayElem eTy (n-1)
        -- the array has not the right dimension, 
        -- PHP would return NULL though, but this is the same as an unset variable
        inspect n ty = Top

-- | @'typeOfArray' aTy n eTy@ computes the type of the array that results
-- from assigning something of type @eTy@ to its @n@th dimension.
-- Note that if @aTy@ is not just an array type the type returned
-- will be 'Top', because in PHP a scalar value cannot be turned into an array.
-- However if @aTy@ contains only scalar types then these are returned unchanged.
typeOfArray :: LType -> Int -> LType -> LType
typeOfArray Top _ _ = Top
typeOfArray (AnyOf s) n eTy | S.null s = iterate array eTy !! n
typeOfArray (AnyOf s) n eTy | F.all isScalar s = eTy
typeOfArray (AnyOf s) n eTy | F.all isArray s = f s n
  where   f :: S.Set Type -> Int -> LType
          f tys n = AnyOf others `join` array (g joined n)
            where (arrays, others) = S.partition isArray tys
                  joined = joinLists [ g lt n | TArray lt <- S.toList arrays]
          g :: LType -> Int -> LType
          g Top n = Top
          g (AnyOf s) 1 = AnyOf s `join` eTy
          g (AnyOf s) n = f s (n-1)
typeOfArray (AnyOf s) n eTy = Top

-- | Strangely not included in module Set 
concatMap :: (Ord a, Ord b) => (a -> S.Set b) -> S.Set a -> S.Set b
concatMap f ss = S.foldr (S.union . f) S.empty ss
