module SoftTyping
  ( module SoftTyping.Lattice
  , softTyping
  ) where

import Monotone
import SoftTyping.Lattice
import SoftTyping.Transfer
import SoftTyping.Flow
import Lattice
import qualified Php (labels)
import Php hiding (labels)
import qualified Data.Set as S

softTyping :: Program -> Int -> Monotone LTypeMap Label
softTyping p k = Instance
  { transf  = mkTransfer p
  , flow    = mkFlow p
  , extremal = S.singleton (initLabel p)
  , iota    = bottom
  , labels = S.fromList (Php.labels p)
  , limitContext = take k
  }
