module Lattice where

import qualified Data.Set as S

-- | @'bottom'@ must satisfy the law : 
--      @forall a : a `join` bottom = a@
class Lattice a where
  join   :: a -> a -> a
  -- not sure about the name of this one. this is the <= comparision operator.
  underOrEqual  :: a -> a -> Bool
  
  top    :: a
  bottom :: a

joinSets :: (Lattice a) => S.Set a -> a
joinSets = S.foldr join bottom

joinLists :: (Lattice a) => [a] -> a
joinLists = foldr join bottom
