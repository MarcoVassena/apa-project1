{-# LANGUAGE TypeOperators #-}

module Monotone (
    Edge(..)
  , InterEdge(..)
  , Transfer(..)
  , TransferId(..)
  , Monotone(..)
  , closed
  , open
)where

import Data.Set hiding (map, null, foldr)
import qualified Data.Set as S
import Lattice
import Data.STRef
import Data.Array.ST hiding (index)
import Control.Monad (forM, when)
import Control.Monad.ST.Safe
import qualified Data.Map as M
import qualified Data.Array.IArray as A
import Php.Core (Label)

data Edge a = Step a a
             | Inter a a (InterEdge a) 
  deriving (Show, Ord, Eq)

data InterEdge a = FunCall a a
                 | FunRet a a
  deriving (Show, Ord, Eq)

src :: Edge a -> a
src (Step l _) = l
src (Inter l _ _) = l

dest :: Edge a -> a
dest (Step _ l) = l
dest (Inter _ l _) = l

-- | The transfer functions must be monotone.
data Transfer a = Unary (a -> a) | Binary (a -> a -> a)

-- | An identifier for transfer functions.
data TransferId b = Simple b
                  | Call b b
                  | Return b b
  deriving (Show, Eq, Ord)

data Monotone a b = Instance
  { transf :: TransferId b -> Transfer a
  , flow :: Set (Edge b)
  , extremal :: Set b
  , iota :: a
  , labels :: Set b
  , limitContext :: Context b -> Context b
  }

type Context a = [a]

-- | Syntactic sugar for mapping
type a :-> b = M.Map a b

-- | The mapping used in the embellished algorithm.
-- It consists of two nested mapping, the nested one has context as keys.
type b :~> a = b :-> ((Context b) :-> a)

-- | Computes the intial values of the lattice for analyze.
initialize :: (Lattice a, Ord b) => Monotone a b -> b :~> a
initialize m = M.fromSet (\l -> M.singleton [] (f l)) (labels m)
  where f l | l `member` (extremal m) = iota m
        f l = bottom


open :: (Lattice a, Ord b, Eq b, Show b) => Monotone a b -> b :-> a
open = joinContexts . analyze

closed :: (Lattice a, Ord b, Eq b, Show b) => Monotone a b -> b :-> a
closed m = (joinContexts . f . analyze) m
  where f a = S.foldr apply a (flow m)
        apply e a = M.insert l (M.unionWith join fal al) a
          where l = src e
                al = a M.! l
                fal = applyTransfer e a m


joinContexts :: (Lattice a, Ord b, Eq b) => b :~> a -> b :-> a
joinContexts = M.map (M.foldr join bottom)

-- | Iterative algorithm (MFP) that computes the least fixed point.
analyze :: (Lattice a, Ord b, Eq b, Show b) => Monotone a b -> b :~> a
analyze m = solve (initialize m) workList
  where workList = toList (flow m)
        solve a (e:es) =
          let (l, l') = (src e, dest e)
              al = a M.! l
              al' = a M.! l'
              fal = applyTransfer e a m in
              if not (M.isSubmapOfBy underOrEqual fal al')
                 then solve (M.insert l' (M.unionWith join fal al') a) $ (edgesFrom l') ++ es
                 else solve a es
        solve a [] = a
        edgesFrom from = [ x | x <- toList (flow m), src x == from ]

-- | Applies the transfer function defined in the 'Monotone' instance for the given 'Edge',
-- and change the context accordingly to the kind of edge.
-- If the function found has the wrong arity an error is thrown.
applyTransfer :: (Lattice a, Eq b, Ord b, Show b)
              => Edge b
              -> b :~> a
              -> Monotone a b
              -> (Context b) :-> a

applyTransfer e@(Step l1 l2) a m = M.map f ctxMap -- Lifting pointwise
  where Unary f = getUnary (Simple l1) m
        ctxMap = a M.! (src e)

applyTransfer e@(Inter l1 l2 (FunCall lc lr)) a m = M.map f pushedMap
  where Unary f = getUnary (Call lc lr) m
        pushedMap = M.mapKeysWith join ((limitContext m) . (:) lc) ctxMap
        ctxMap = a M.! (src e)

applyTransfer e@(Inter l1 l2 (FunRet lc lr)) a m = M.fromList merged
  where Binary f2 = getBinary (Return lc lr) m
        ctxMap = a M.! (src e)
        hasPendingCall (l:_) _ = l == lc
        hasPendingCall [] _ = False
        pendingCallMap = M.filterWithKey hasPendingCall ctxMap
        beforeCallMap ls = M.filterWithKey (\k _ -> k == ls) (a M.! lc)
        merged = if M.null pendingCallMap 
                  -- Context insensitive
                  then [([], f2 x y) | x <- M.elems (a M.! lc),
                                       y <- M.elems ctxMap ]
                  -- Context sensitive
                  else [(ctx, f2 x y) | ((_ : ctx), y) <- M.toList pendingCallMap,
                                                     x <- M.elems (beforeCallMap ctx)]
                         
getUnary :: Show b => TransferId b -> Monotone a b -> Transfer a
getUnary tId m =
  case transf m tId of
    Unary f -> Unary f
    Binary _ -> error $ "applyTransfer: Given Binary function, Unary expected for " ++ show tId

getBinary :: Show b => TransferId b -> Monotone a b -> Transfer a
getBinary tId m =
  case transf m tId of
    Unary _ -> error $ "applyTransfer: Given Unary function, Binary expected for " ++ show tId
    Binary f2 -> Binary f2
