module Php.Convert.Labeling (label) where

import qualified Lang.Php.Ast as L

import Control.Monad.State
import Php.Core
import Php.Convert.Base

type LabelM = State [Label]

label :: Program -> Program 
label (Program funs b) = evalState m [0..]
  where m = do
          funs' <- mapM label' funs
          b' <- label' b
          return $ Program funs' b'

newLabel :: LabelM Label
newLabel = do
  (l:ls) <- get
  put ls
  return l

class Labelable a where
  label' :: a -> LabelM a

instance Labelable Stmt where
  label' (Ite _ e s1 s2) = do
    l <- newLabel
    s1' <- label' s1
    s2' <- label' s2
    return $ Ite l e s1' s2'
  label' (While _ e b) = do
    l <- newLabel
    b' <- label' b
    return $ While l e b'
  label' (Asgn _ e1 e2) = newLabel >>= \l -> return $ Asgn l e1 e2
  label' (Skip _)       = newLabel >>= return . Skip
  label' (Goto _ jl)    = newLabel >>= \l -> return $ (Goto l jl)
  label' (JLabel _ jl)  = newLabel >>= \l -> return $ (JLabel l jl)
  label' (Poison _)     = newLabel >>= return . Poison
  label' (Call _ _ e i es)  = do
    l1 <- newLabel
    l2 <- newLabel
    return $ Call l1 l2 e i es
  label' (Seq s1 s2) = liftM2 Seq (label' s1) (label' s2)
  label' (Global _ names) = newLabel >>= \l -> return $ Global l names

instance Labelable FunDef where
  label' (FunDef _ _ i v vs b) = do
     l1 <- newLabel
     b' <- label' b
     l2 <- newLabel
     return $ FunDef l1 l2 i v vs b'
