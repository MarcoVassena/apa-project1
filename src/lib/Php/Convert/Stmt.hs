module Php.Convert.Stmt where

import qualified Lang.Php.Ast as L

import Control.Monad.State
import Php.Core
import Php.Convert.Base
import Php.Convert.Flatten

class ToStmt a where
  toStmt :: a -> Conv Stmt

instance ToStmt L.Stmt where
  toStmt (L.StmtBreak e _ _) = do
    let n = case e of
          (Just _) -> error "Not implemented"
          Nothing  -> 1
    trg <- getBreakTarget n
    return $ Goto 0 trg

  toStmt (L.StmtDoWhile (L.DoWhile body e _)) = do
    (s, e) <- flatten' e
    b <- toStmt body
    let body' = b >>> s
    return $ body' >>> (While 0 e body')
    
  toStmt (L.StmtEcho es _) = do
    fs <- mapM flatten' es
    let (ss, _) = unzip fs
    -- We don't translate the echo itself, as we do not care about it.
    -- We evaluate the expressions and immediately throw away the result however.
    return $ seqs ss

  toStmt (L.StmtExpr e _ _) = flatten' e >>= return . fst


  toStmt (L.StmtFor (L.For (L.WSCap _ (es1, es2, es3) _) body)) = do
    -- TODO refactoring
    let flatten1 (L.ForPart (Left _ )) = return (Skip 0, [])
        flatten1 (L.ForPart (Right es)) = runFlatten $ mapM flatten es 
    (s1, _) <- flatten1 es1
    (s2, es2') <- flatten1 es2
    (s3, _) <- flatten1 es3

    b <- toStmt body

    let guard = if null es2' then true else last es2'
        body' = b >>> s3 >>> s2
    return $ s1 >>> s2 >>> (While 0 guard body')

  toStmt (L.StmtFuncDef (L.Func _ _ name (L.WSCap _ as _) s)) = do
    let as' = either (const []) (map h) as
    res <- newAuxV
    s' <- withResultVar res (toStmt s)
    addFunDef (FunDef 0 0 name res as' s')
    return $ Skip 0
    where h :: L.WSCap L.FuncArg -> Var
          h (L.WSCap _ (L.FuncArg _ _ (L.VarMbVal (L.Var v _) Nothing)) _) = UserV v

  toStmt (L.StmtIf (L.If _ ibs eb)) = do -- TODO check
    let ibs' = icToList1 ibs --if and elseif branches

    eb' <- maybe (return (Skip 0)) (toStmt . snd) eb

    foldM h eb' (reverse ibs')

    where
      h :: Stmt -> L.IfBlock -> Conv Stmt
      h a (L.IfBlock g b) = do
        (s, g') <- flatten' g
        b' <- toStmt b  
        return $ s >>> (Ite 0 g' b' a)

  toStmt (L.StmtNothing _) = return $ Skip 0
  toStmt (L.StmtReturn _  val _) = do
    (s, e') <- case val of
                 Nothing -> return (Skip 0, LitNull)
                 (Just (e, _)) -> flatten' e
    res <- getResultVar
    return $ s >>> (res <--- e')

  toStmt (L.StmtSwitch (L.Switch e _ cs)) = do -- TODO: does php evaluate case epxr on fall-through?
    (s, e') <- flatten' e

    -- add a label for each case
    ls <- mapM (const newJLabel) cs
    end <- newJLabel

    casc <- foldM (g e') (Skip 0) (zip cs ls)

    code <- withBreakTarget end $ foldM h (Skip 0) (zip cs ls)
    let goto = Goto 0 end
        jlabel = JLabel 0 end
    return $ s >>> casc >>> goto >>> code >>> jlabel 
    where
      g :: Expr -> Stmt -> (L.Case, JumpLabel) -> Conv Stmt
      g e' a ((L.Case (Left _)  _), lbl)  = return $ a >>> Goto 0 lbl -- assuming that this is the default case?
      g e' a ((L.Case (Right g) _), lbl) = do
        (sg, g) <- flatten' g
        let goto = Goto 0 lbl
            skip = Skip 0
            ite  = Ite 0 (BinExpr BEQ e' g) goto skip
        return $ a >>> sg >>> ite

      h :: Stmt -> (L.Case, JumpLabel) -> Conv Stmt
      h a ((L.Case _ b), lbl) = do
        b' <- toStmt $ icToList2 b
        let jlabel = JLabel 0 lbl
        return $ a >>> jlabel >>> b'

  toStmt (L.StmtWhile (L.While e body)) = do
    (s, e) <- flatten' e
    b <- toStmt body
    let while = While 0 e (b >>> s)
    return $ s >>> while

  toStmt (L.StmtGlobal vars _) = return $ Global 0 vs
    where vs = map (getName . L.wsCapMain) vars
          getName (L.Var name []) = name
          getName v = error $ "toStmt: Unsupported var in global " ++ show v

--  toStmt (L.StmtForeach f) = undefined  -- This looks complicated. Key value pairs etc.
  toStmt stm = error $ "toStmt: Unsupported construct " ++ show stm

instance (ToStmt a) => ToStmt (Maybe a) where
  toStmt (Just s) = toStmt s
  toStmt Nothing  = return $ Skip 0

instance (ToStmt a, ToStmt b) => ToStmt (Either a b) where
  toStmt (Left l)  = toStmt l
  toStmt (Right r) = toStmt r

instance (ToStmt a) => ToStmt (L.Block a) where
  toStmt (L.Block ic) = toStmt $ icToList2 ic

instance (ToStmt a) => ToStmt [a] where
  toStmt [] = return $ Skip 0
  toStmt (x:xs) = liftM2 (>>>) (toStmt x) (toStmt xs)

instance (ToStmt a) => ToStmt (L.WSCap a) where
  toStmt = toStmt . L.wsCapMain


