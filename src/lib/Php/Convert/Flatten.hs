module Php.Convert.Flatten where

import qualified Lang.Php.Ast as L

import Control.Monad.State
import Data.CaseInsensitive
import Php.Core
import Php.Convert.Base
import System.IO.Unsafe (unsafePerformIO)

--------------------------------------------------------------------------------
-- Flatten monad
--------------------------------------------------------------------------------

type Flat = Conv

flatten' :: (Flatten a) => a -> Conv (Stmt, Expr)
flatten' e = runFlatten (flatten e)

runFlatten :: (Flat a) -> Conv (Stmt, a)
runFlatten f = do
  st <- get
  let (r, s) = runState f st
  put (st { freshVarId = freshVarId s })
  return (seqs (preStmts s), r)

-- Helper functions to modify 'CState'

-- Appends a 'Stmt' in the 'preStmts' field.
append :: Stmt -> CState -> CState
append x fs = fs { preStmts = xs ++ [x] } 
  where xs = preStmts fs

-- Consume the current fresh variable identifier
succAuxV :: CState -> CState
succAuxV fs = fs { freshVarId = succ fId }
  where fId = freshVarId fs

-- Append a Stmt to the state
pre :: Stmt -> Flat ()
pre x = modify (append x)

-- Introduces a new auxiliary variables
newAuxV :: Flat Var
newAuxV = do
  newId <- gets freshVarId 
  modify succAuxV
  return $ AuxV newId

returnVar :: Var -> Flat Expr
returnVar = return . Variable

--------------------------------------------------------------------------------

class Flatten a where
  flatten :: a -> Flat Expr

instance Flatten L.Val where
  flatten (L.ValLOnlyVal lOnlyVal) = flatten lOnlyVal
  flatten (L.ValROnlyVal rOnlyVal) = flatten rOnlyVal
  flatten (L.ValLRVal  lrVal) = flatten lrVal

instance Flatten L.LVal where
  flatten (L.LValLOnlyVal lhs) = flatten lhs
  flatten (L.LValLRVal lrVal) = flatten lrVal

instance Flatten L.RVal where
  flatten (L.RValROnlyVal rOnlyVal) = flatten rOnlyVal
  flatten (L.RValLRVal lrVal) = flatten lrVal

instance Flatten L.LRVal where
  flatten (L.LRValVar (L.DynConst [] v)) = flatten v
  -- I don't know what it is exactly, on the safe side abort.
  flatten (L.LRValVar (L.DynConst xs _)) = error $ "flatten: Non empty dynamic context " ++ show xs 
  flatten lv = error $ "flatten: Unsupported left value " ++ show lv

-- It could be array access
instance Flatten L.LOnlyVal where
  flatten lhs = error $ "flatten: unsupported LOnlyVal " ++ show lhs

-- I think this are function calls
instance Flatten L.ROnlyVal where
  flatten (L.ROnlyValFunc (Right (L.Const _ f)) _ args) = do
    auxV <- newAuxV
    let args' = either (const []) id args
    es' <- mapM flatten args'

    pre $ Call 0 0 auxV f es'
    returnVar auxV
  flatten (L.ROnlyValConst c) = flatten c
  flatten rv = error $ "flatten: unsupported ROnlyVal " ++ show rv

instance Flatten L.Var where
  flatten (L.Var iden []) = return $ Variable (UserV iden)
  flatten lv@(L.Var iden xs) = liftM (Array iden) (mapM f xs)
    where f (_, (_,x)) = flatten x
  flatten v = error $ "flatten: Unsupported dynamic variables " ++ show v

instance Flatten L.Const where
  flatten (L.Const [] s) = 
    let known = [(mk "true", LitBool True), (mk "false", LitBool False), (mk "null", LitNull)]
        found = lookup (mk s) known in
    case found of 
      Just r -> return r
      Nothing -> error $ "User defined constants (" ++ s ++ ") not supported"
    
  flatten c@(L.Const ctx s) = error $ "Non-empty context in constant " ++ show c

instance Flatten L.Expr where
  flatten (L.ExprAssign Nothing lv _ e) = do
    e' <- flatten e
    lhs <- flatten lv
    pre (lhs <-- e')
    return lhs

  flatten (L.ExprAssign (Just bop) lv _ e) = do 
    e' <- flatten e
    lv' <- flatten lv
    pre (lv' <-- BinExpr (toBinOp bop) lv' e')
    return lv'

  flatten (L.ExprEval _ e) = do
    auxV <- newAuxV
    e' <- flatten e
    -- we don't care about the actual value...
    pre (Poison 0)
    -- return magic value (we don't know the type...)
    return Magic

  flatten (L.ExprTernaryIf (L.TernaryIf cond _ me1 _ e2)) = do
      cond' <- flatten cond
      case me1 of
        Just e1 -> do
          e1' <- flatten e1
          auxV <- newAuxV
          e2' <- flatten e2
          pre (Ite 0 cond' (auxV <--- e1') (auxV <--- e2'))
          returnVar auxV 
        Nothing -> do
          auxV <- newAuxV
          e2' <- flatten e2
          pre (Ite 0 cond' (Skip 0) (auxV <--- e2'))
          returnVar auxV

    -- Here pattern matching on bop we could implement short-circuit evaluation
  flatten (L.ExprBinOp bop e1 _ e2) = do
     e1' <- flatten e1
     e2' <- flatten e2
     auxV <- newAuxV
     pre (auxV <--- BinExpr (toBinOp bop) e1' e2')
     returnVar auxV

  flatten (L.ExprPreOp preOp _ e) = do
      e' <- flatten e
      auxV <- newAuxV
      pre (auxV <--- UnExpr (toUnOp preOp) e')
      returnVar auxV

  flatten (L.ExprPostOp postOp e _) = do
      e' <- flatten e
      auxV1 <- newAuxV
      pre (auxV1 <--- UnExpr (toUnOp postOp) e')
      returnVar auxV1

-- TODO: This should not be difficult to implement
  flatten (L.ExprBackticks e ) = error $ "flatten: Unsupported construct " ++ show e
  flatten (L.ExprHereDoc (L.HereDoc s)) = return (LitString s)

  -- TODO Check:
  -- 1) I don't know whether we should distinguish between include and require 
  --    Require could fail (`type crash').
  --
  -- 2) For dynamic includes should we fail or simply go to top?
  --    For static analysis shall we actually include the file and propagate the analysis?
  --
  -- 3) I don't think for soft-typing analysis it matters the difference between 
  --    include_once and require_once.
  flatten (L.ExprInclude _ _ _ e) = do
    e' <- flatten e
    case e' of
      LitString s -> do
        -- inline included file
        parser <- gets cs_parse
        -- This is safe, because we assume that files are not changing while this program is run.
        -- TODO somehow the string lit contains the enclosing quotes as well. Looks like a
        -- parser bug. The init $ tail stuff is the work around. Shouldn't make any difference for the other parts.
        let (L.Ast _ ast) = unsafePerformIO $ parser $ init $ tail s
        toSt <- gets cs_toStmt
        stm' <- toSt $ icToList2 ast
        pre stm'
        -- php include can return abitrary things
        return Magic
      _ -> do
        -- non-statically known include, add poison
        pre $ Poison 0
        return Magic

  flatten (L.ExprNumLit (L.NumLit s)) = return $ LitNum s
  flatten (L.ExprParen wsExpr ) = flatten wsExpr
  flatten (L.ExprRVal rVal) = flatten rVal
  flatten (L.ExprStrLit (L.StrLit s)) = return (LitString s)
  flatten expr  = error $ "flatten: Unsupported construct " ++ show expr

instance (Flatten a, Flatten b) => Flatten (Either a b) where
  flatten (Left a) = flatten a
  flatten (Right b) = flatten b

instance (Flatten a) => Flatten (L.WSCap a) where
  flatten = flatten . L.wsCapMain
