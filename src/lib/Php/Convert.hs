-- | A minimal selection of constructs are defined onto which 
-- all Php constructs are mapped, taking care not to change
-- the semantics.

module Php.Convert where

import qualified Lang.Php.Ast as L

import qualified Parser as P
import Php.Core
import Php.Convert.Base
import Php.Convert.Labeling (label)
import Php.Convert.Stmt (toStmt)
import Control.Monad (foldM)
import Control.Monad.Trans.State

-- | Convert 'Ast' to the core language.
-- Since we support only few constructs the default behaviour for unsupported 
-- features is to fail during the conversion.

toCore :: L.Ast -> Program
toCore (L.Ast _ stmts) = label $ Program (funDefs cStateFinal) block
  where cState = CState 0 0 Nothing [] [0..] Nothing P.parseFile toStmt []
        (block, cStateFinal) = runState (toStmt (icToList2 stmts)) cState 
