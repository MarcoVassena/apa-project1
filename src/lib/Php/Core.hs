-- | This module defines the essential aspects of Php.

module Php.Core where

import qualified Lang.Php.Ast as L
import Control.Monad.Trans.State
import qualified Data.Intercal as IC

type Label = Int

type Identifier = String

type JumpLabel = Int

data Program = Program FunDefs Stmt
  deriving (Show)

-- | The first 'Var' stores the result of the function
data FunDef = FunDef Label Label Identifier Var [Var] Stmt
  deriving (Show, Eq)

type FunDefs = [FunDef]

data Stmt = 
    Ite     Label Expr Stmt Stmt
  | While   Label Expr Stmt
  | Seq     Stmt  Stmt
  | Asgn    Label Expr Expr
  | Skip    Label
  | Goto    Label JumpLabel 
  | JLabel  Label JumpLabel
  | Poison  Label -- Forget everything about the variables, anything might happened
  | Call    Label Label Var Identifier [Expr]  -- ^ Function call and assigning the return value.
  | Global  Label [Identifier]  -- ^ Puts into scope global variables
 deriving (Show, Eq)

data Expr = 
    LitString String
  | LitNum    String
  | LitBool   Bool
  | LitNull
  | Variable  Var
  | Array     Identifier [Expr]   -- ^ Access to an array at the index resulting from the given expr
  | BinExpr   BinOp Expr Expr
  | UnExpr    UnOp Expr
  | Magic                     -- ^ Has any type (like undefined in Haskell)
 deriving (Show, Eq)

-- TODO I think we will need to add something to UserV to model also arrays
data Var = UserV Identifier -- ^ User defined variables
         | AuxV Int         -- ^ Auxiliary variables
  deriving (Eq, Ord)

-- TODO Logical operators (&&, ||) must be removed, and translated
-- properly to deal with short circuit

-- Binary Operators
data BinOp = BAnd | BEQ | BSEQ {-Strict EQ === -} | BGE | BGT | BID | BLE | BLT | BNE | BOr 
           | BNI | BConcat | BDiv | BMinus | BMod | BMul | BPlus
  deriving (Show, Eq)

-- Unary Operators
data UnOp = UNot | PreIncr | PreDecr | PostIncr | PostDecr
  deriving (Show, Eq)

-- Helper functions to deal with operators

isArithmetic :: BinOp -> Bool
isArithmetic bop = elem bop [BDiv, BMinus, BMod, BMul, BPlus]

isComparison :: BinOp -> Bool
isComparison bop = elem bop [BEQ, BSEQ, BGE, BGT, BID, BLE, BLT, BNE, BNI]

isLogical :: BinOp -> Bool
isLogical bop = elem bop [BAnd, BOr]

isPostOp :: UnOp -> Bool
isPostOp uop = elem uop [PostIncr, PostDecr]

isPreOp :: UnOp -> Bool
isPreOp uop = elem uop [PreIncr, PreDecr]

-- Shorthands

(<--) :: Expr -> Expr -> Stmt
(<--) = Asgn 0

-- Assignment to 'Variable' only.
(<---) :: Var -> Expr -> Stmt
v <--- e = Asgn 0 (Variable v) e

(>>>) :: Stmt -> Stmt -> Stmt
s1 >>> s2 = Seq s1 s2

seqs :: [Stmt] -> Stmt
seqs = foldr Seq (Skip 0)

true :: Expr
true = LitBool True

false :: Expr
false = LitBool False

class HasLabel a where
  -- | Returns the initial 'Label'
  initLabel :: a -> Label
  -- | Returns the set of final labels
  final :: a -> [Label]
  -- Returns all the labels present
  labels :: a -> [Label]

instance HasLabel Stmt where
  initLabel (Ite lbl e stmt1 stmt2) = lbl
  initLabel (While lbl e stmt) = lbl
  initLabel (Asgn lbl e1 e2) = lbl
  initLabel (Skip lbl) = lbl
  initLabel (Goto lbl jlbl) = lbl
  initLabel (JLabel lbl jlbl) = lbl
  initLabel (Poison lbl) = lbl
  initLabel (Call l1 l2 _ _ _) = l1
  initLabel (Seq s1 s2) = initLabel s1
  initLabel (Global lbl names) = lbl

  final (Ite lbl e b1 b2) = final b1 ++ final b2
  final (While lbl e b) = [lbl]
  final (Asgn lbl e1 e2) = [lbl]
  final (Skip lbl) = [lbl]
  final (Goto lbl  jlbl) = []
  final (JLabel lbl jlbl) = [lbl]
  final (Poison lbl) = [lbl]
  final (Call l1 l2 _ _ _) = [l2]
  final (Seq s1 s2) = final s2
  final (Global lbl names) = [lbl]

  labels (Ite lbl e b1 b2) = labels b1 ++ labels b2 ++ [lbl]
  labels (While lbl e b) = labels b ++ [lbl]
  labels (Asgn lbl e1 e2) = [lbl]
  labels (Skip lbl) = [lbl]
  labels (Goto lbl  jlbl) = [lbl]
  labels (JLabel lbl jlbl) = [lbl]
  labels (Poison lbl) = [lbl]
  labels (Call l1 l2 _ _ _) = [l1,l2]
  labels (Seq s1 s2) = labels s1 ++ labels s2
  labels (Global lbl names) = [lbl]

instance HasLabel Program where
  initLabel (Program fs s) = initLabel s
  final (Program fs s) = final s
  labels (Program fs s) = concatMap labels fs ++ labels s

instance HasLabel FunDef where
  initLabel (FunDef l1 _ _ _  _ _) = l1
  final (FunDef _ l2 _ _ _ _) = [l2]
  labels (FunDef l1 l2 _ _ _ s) = [l1, l2] ++ labels s

instance Show Var where
  show (UserV v) = "user_" ++ v 
  show (AuxV v)  = "aux_" ++ (show v) 

-- | Returns the body of a function definition
body :: FunDef -> Stmt
body (FunDef _ _ _ _ _ b) = b
