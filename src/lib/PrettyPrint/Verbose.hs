-- | Prints the 'Core' AST and additional information.

{-# LANGUAGE FlexibleInstances, DoAndIfThenElse #-}

module PrettyPrint.Verbose where

import Php
import PrettyPrint.Simple hiding (block)
import SoftTyping
import Data.Map hiding (map)
import qualified Data.Map as M
import Data.List (intercalate)
import Control.Monad.State

type PM = State (Maybe LTypeMap)

pprintWith :: PPrintWith a => Map Label LTypeMap -> a -> String
pprintWith lt p = evalState (pprintWith' lt p) Nothing

class (PPrint a) => PPrintWith a where
  pprintWith' :: (PPrint a) => Map Label LTypeMap -> a -> PM String

instance PPrintWith Program where
  pprintWith' lt (Program funs s) = do
    ppFuns <- mapM (pprintWith' lt) funs >>= return . concat
    ppS <- pprintWith' lt s
    return $ ppFuns ++ ppS

instance PPrintWith Stmt where

  pprintWith' lt s@(Call l1 l2 a i es) = do
    put Nothing
    t1 <- printEntryFor l1 lt
    put Nothing
    t2 <- printEntryFor l2 lt
    return $ t1 ++ "\n" ++  pprint s ++ "\n" ++ t2

  pprintWith' lt (Ite l e b1 b2) = do
    entry <- printEntryFor l lt
    let stmt = "if (" ++ pprint e ++ ")" ++ pLbl l ++ block lt b1 ++ " else " ++ block lt b2
    return $ unlines [entry, stmt]

  pprintWith' lt (While l e b) = do
    let stmt = "while (" ++ pprint e ++ ")" ++ pLbl l ++ block lt b
    entry <- printEntryFor l lt
    return $ unlines [entry, stmt]
 
  pprintWith' lt (Seq s1 s2) = pprintWith' lt s1 >>= \a -> pprintWith' lt s2 >>= \x -> return (a ++ "\n" ++ x)
  pprintWith' lt s = printEntryFor (initLabel s) lt >>= return . ((pprint s ++ "\n") ++)

instance PPrintWith FunDef where
  pprintWith' lt s@(FunDef l1 l2 i res vs b) = do
    let header = "fun " ++ i ++ "(" ++ sepByComma vs ++ ") " ++ pprint res ++ pLbl l1
    let body = block lt b ++ pLbl l2
    entry1 <- printEntryFor l1 lt
    entry2 <- printEntryFor l2 lt
    return $ unlines [entry1, header, body, entry2]

-- | If an entry is present in the mapping for the given label 
-- its representation is returned. Otherwise the empty string is returned.
printEntryFor :: Label -> Map Label LTypeMap -> PM String
printEntryFor lbl lt = do
  last <- get
  let x = M.lookup lbl lt
  if last == x then
    return ""
  else do
    put x
    return $ maybe "" (\y -> show y) x

-- | Prints a block with '{' '}' and proper indentation
block :: Map Label LTypeMap -> Stmt -> String
block lt stmt = "{\n" ++ indent (pprintWith lt stmt) ++ "}"
