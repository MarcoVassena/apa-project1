-- Prints the core AST in a readable fashion

{-# LANGUAGE FlexibleInstances #-}

module PrettyPrint.Simple (
    pprint
  , PPrint
  , pLbl
  , indent
  , sepByComma
  , 
  ) where

import Php
import SoftTyping
import Data.Map hiding (map)
import qualified Data.Map as M
import Data.List (intercalate)

class PPrint a where
  pprint :: a -> String

instance PPrint Program where
  pprint (Program funs s) = concatMap pprint funs ++ pprint s

instance PPrint Stmt where
  pprint (Ite l e b1 b2) = "if (" ++ pprint e ++ ")" ++ pLbl l ++ block b1 ++ " else " ++ block b2
  pprint (While l e b) = "while (" ++ pprint e ++ ")" ++ pLbl l ++ block b
  pprint (Asgn l e1 e2) = pprint e1 ++ " <- " ++ pprint e2 ++ pLbl l 
  pprint (Skip l) = "skip " ++ pLbl l
  pprint (Goto lbl jlbl) = "goto " ++ show jlbl ++ pLbl lbl
  pprint (JLabel lbl jlbl) = "label " ++ show jlbl ++ pLbl lbl
  pprint (Poison lbl) = "poison " ++ pLbl lbl 
  pprint (Call l1 l2 a i es) = show a ++ " <- " ++ i ++ "(" ++ sepByComma es ++ ")" ++ pLbl l1 ++ pLbl l2
  pprint (Seq s1 s2) = pprint s1 ++ "\n" ++  pprint s2
  pprint (Global l names) = "global " ++ sepByComma names

instance PPrint FunDef where
  pprint (FunDef l1 l2 i res vs b) = unlines [header, body] 
    where header = "fun " ++ i ++ "(" ++ sepByComma vs ++ ") " ++ pprint res ++ pLbl l1 
          body = block b ++ pLbl l2

-- Prints a label within square brackets.
pLbl :: Label -> String
pLbl l = " [" ++ show l ++ "]"

-- Prints a comma separated list of PPrintable things.
sepByComma :: (PPrint a) => [a] -> String
sepByComma es = intercalate ", " $ map pprint es

instance PPrint Expr where
  pprint (LitString s)  = s
  pprint (LitNum s)     = s 
  pprint (Variable v)  = pprint v
  pprint (BinExpr op e1 e2) = "(" ++ pprint e1 ++ ") " ++ pprint op ++ " (" ++ pprint e2 ++ ")"
--  pprint (UnExpr  op e) =
  pprint (Magic)        = "magic"
  pprint e = show e

instance PPrint BinOp where
  pprint BEQ = "=="
  pprint BGE = ">="
  pprint BGT = ">"
  pprint BLE = "<="
  pprint BLT = "<"
  pprint BPlus = "+"
  pprint BMinus = "-"
  pprint o = show o

instance PPrint Var where
  pprint = show

instance PPrint Identifier where
  pprint s = s

-- | Prints a block with '{' '}' and proper indentation
block :: Stmt -> String
block stmt = "{\n" ++ indent (pprint stmt) ++ "}"

-- | Provides indentation
indent :: String -> String
indent = unlines . map ("  " ++) . lines

instance PPrint (Map Label LTypeMap) where 
  pprint m = unlines $ map f (toList m)
    where f (l, lt) = pLbl l ++ " -> " ++ show lt
