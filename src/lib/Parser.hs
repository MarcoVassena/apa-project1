module Parser where

import qualified Text.Parsec.Prim as P
import LexPassUtil
import Parse
import Unparse
import Lang.Php.Ast
import System.Directory

parseFile :: String -> IO Ast
parseFile f = do
  ts <- readSrcFile f
  case P.runParser parse () f ts of
        Left err -> error $ show err
        Right ast -> do
          return ast


