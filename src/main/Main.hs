module Main where

import Parser
import Options.Applicative
import Php
import PrettyPrint
import SoftTyping hiding (showDefault)
import Monotone
import System.Directory
import System.FilePath

data Options = Options
  { file :: String,
    k :: Int
  }

options :: Parser Options
options = Options
  <$> argument str (metavar "<FILE>")
  <*> option (short 'k' <> value 3
                        <> metavar "N"
                        <> help "Maximum length of the context"
                        <> showDefault)
  
pOpts :: ParserInfo Options
pOpts = info (helper <*> options)
  (fullDesc <> progDesc "PHP soft-typing analyser.")

main :: IO ()
main = do
  opts <- execParser pOpts

  let (dir, fil) = splitFileName (file opts)
  putStrLn $ "k = " ++ show (k opts)
  putStrLn $ "Working dir: " ++ dir
  setCurrentDirectory dir
  putStrLn "\n\n"
  
  ast <- parseFile fil
--  putStrLn "Lexpass Ast:"
--  putStrLn $ show ast
--  putStrLn "\n\n"

  let core = toCore ast
  putStrLn "CORE Ast:"
  putStrLn $ pprint core
  putStrLn "\n\n"

  let stInst = softTyping core (k opts)
      cl = closed stInst


  putStrLn "Flow:"
  putStrLn $ show $ flow stInst
  putStrLn "\n\n"
  
  putStrLn "Analysis result (closed):"
  putStrLn $ pprint cl
  putStrLn "\n\n"

  putStrLn "CORE Ast with result (closed):"
  putStrLn $ pprintWith cl core
  
  return ()
