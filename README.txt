APA Project 1


Authors: Marco Vassena,    4110161
         Philipp Hausmann, 4003373



===========
BUILDING
===========

Prerequisites (other versions might work too):
 * GHC 7.6.3
 * cabal 1.16.0.2

Special dependencies:
 * lex-pass from https://github.com/phile314/lex-pass


Building:
$ cabal configure --enable-tests
$ cabal build


Running the program:
$ ./dist/build/main/main <FILE> [OPTIONS]
where FILE is the path to a PHP file.

Get help about available options:
$ ./dist/build/main/main --help



Running the tests:
$ cabal test

==========
Examples
==========

See the examples/ directory. 

==========
DOCUMENTATION
==========

See doc/report.pdf for the complete documentation.
See also the haddock documentation (dist/doc/).
