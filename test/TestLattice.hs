{-# LANGUAGE FlexibleInstances #-}
module Main where

import Test.QuickCheck
import Test.QuickCheck.Test
import Control.Monad hiding (join)
import qualified Data.Set as S
import qualified Data.Map as M
import System.Exit
import SoftTyping.Lattice
import Lattice
import Php.Core

-- The maximum number of variables used
maxVars :: Int
maxVars = 4

instance Arbitrary Type where
  --TODO add arrays
  arbitrary = elements $ [TBool, TString, TNum, TNull]

instance Arbitrary LType where
  arbitrary = frequency [(1, top), (5, anyOf)]
    where top = return Top
          anyOf = do
                  tys <- listOf arbitrary
                  return $ AnyOf (S.fromList tys)

instance Arbitrary LTypeMap where
  arbitrary = liftM2 mkTypeMap topOrEmpty arbitrary
    where topOrEmpty = oneof [return Top, return $ AnyOf S.empty]

instance Arbitrary Var where
  arbitrary = liftM AuxV (elements [1..maxVars])

instance Arbitrary (M.Map Var LType) where
  arbitrary = frequency [(1, return M.empty), (2, f)]
    where f = do
              n <- elements [1..maxVars]
              vars <- vectorOf n arbitrary
              tys <- vectorOf n arbitrary
              return $ M.fromList $ zip vars tys
 
-- | Does join imply underOrEqual and viceversa ?
joinEqUnderOrEqual :: LTypeMap -> LTypeMap -> Gen Prop
joinEqUnderOrEqual m1 m2 = label "under<=>join" $ (underP ==> joinP) .&&. (joinP ==> underP)
  where underP = m1 `underOrEqual` m2 
        joinP = m1 `join` m2 == m2

joinAssoc :: LTypeMap -> LTypeMap -> LTypeMap -> Gen Prop
joinAssoc m1 m2 m3 = label "join-associative" prop
  where prop = (m1 `join` m2) `join` m3 == m1 `join` (m2 `join` m3)

joinComm :: LTypeMap -> LTypeMap -> Gen Prop
joinComm m1 m2 =  label "join-commutative" prop
  where prop = m1 `join` m2 == m2 `join` m1

main :: IO ()
main = do
  r1 <- quickCheckResult joinEqUnderOrEqual
  r2 <- quickCheckResult joinAssoc
  r3 <- quickCheckResult joinComm
  if all isSuccess [r1, r2, r3]
    then return ()
    else exitFailure

